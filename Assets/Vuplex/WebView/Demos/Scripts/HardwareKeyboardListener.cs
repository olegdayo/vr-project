/**
* Copyright (c) 2020 Vuplex Inc. All rights reserved.
*
* Licensed under the Vuplex Commercial Software Library License, you may
* not use this file except in compliance with the License. You may obtain
* a copy of the License at
*
*     https://vuplex.com/commercial-library-license
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
using System;
using UnityEngine;

namespace Vuplex.WebView.Demos {

    /// <summary>
    /// Script that detects keys pressed on the hardware keyboard
    /// and emits the corresponding strings that
    /// can be passed to `IWebView.HandleKeyboardInput()`.
    /// </summary>
    class HardwareKeyboardListener : MonoBehaviour {

        public EventHandler<KeyboardInputEventArgs> InputReceived;

        public static HardwareKeyboardListener Instantiate() {

            return (HardwareKeyboardListener) new GameObject("HardwareKeyboardListener").AddComponent<HardwareKeyboardListener>();
        }

        static readonly string[] _keyNames = new string[] {
            "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "`", "-", "=", "[", "]", "\\", ";", "'", ",", ".", "/", "Space", "Enter", "Backspace", "Tab"
        };

        bool _dispatchSpecialKeys(EventHandler<KeyboardInputEventArgs> handler, KeyModifier modifiers) {

            var specialKeyDispatched = false;
            if (Input.GetKeyDown(KeyCode.UpArrow)) {
                handler(this, new KeyboardInputEventArgs("ArrowUp", modifiers));
                specialKeyDispatched = true;
            }
            if (Input.GetKeyDown(KeyCode.DownArrow)) {
                handler(this, new KeyboardInputEventArgs("ArrowDown", modifiers));
                specialKeyDispatched = true;
            }
            if (Input.GetKeyDown(KeyCode.RightArrow)) {
                handler(this, new KeyboardInputEventArgs("ArrowRight", modifiers));
                specialKeyDispatched = true;
            }
            if (Input.GetKeyDown(KeyCode.LeftArrow)) {
                handler(this, new KeyboardInputEventArgs("ArrowLeft", modifiers));
                specialKeyDispatched = true;
            }
            return specialKeyDispatched;
        }

        KeyModifier _getModifiers() {

            var modifiers = KeyModifier.None;
            if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift)) {
                modifiers |= KeyModifier.Shift;
            }
            if (Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl)) {
                modifiers |= KeyModifier.Control;
            }
            if (Input.GetKey(KeyCode.LeftAlt) || Input.GetKey(KeyCode.RightAlt)) {
                modifiers |= KeyModifier.Alt;
            }
            if (Input.GetKey(KeyCode.LeftWindows) ||
                Input.GetKey(KeyCode.RightWindows)) {
                modifiers |= KeyModifier.Meta;
            }
            // Don't pay attention to the command keys on Windows because Unity has a bug 
            // where it falsly reports the command keys are pressed after switching languages
            // with the windows+space shortcut.
            #if !(UNITY_STANDALONE_WIN || UNITY_EDITOR_WIN)
                if (Input.GetKey(KeyCode.LeftCommand) ||
                    Input.GetKey(KeyCode.RightCommand)) {
                    modifiers |= KeyModifier.Meta;
                }
            #endif

            return modifiers;
        }

        void Update() {

            var handler = InputReceived;
            if (handler == null || !(Input.anyKeyDown || Input.inputString.Length > 0)) {
                return;
            }
            var modifiers = _getModifiers();
            var specialKeyDispatched = _dispatchSpecialKeys(handler, modifiers);

            if (specialKeyDispatched) {
                // Input.inputString can contain weird unexpected characters when special
                // characters like the arrow keys are pressed. So, ignore Input.inputString
                // in that case.
                return;
            }

            if (!(modifiers == KeyModifier.None || modifiers == KeyModifier.Shift)) {
                // On Windows, when modifier keys are held down, Input.inputString is
                // blank even if other keys are pressed. So, use Input.GetKeyDown() in
                // that scenario.
                foreach (var key in _keyNames) {
                    if (Input.GetKeyDown(key.ToLower())) {
                        handler(this, new KeyboardInputEventArgs(key, modifiers));
                    }
                }
                return;
            }

            foreach (var character in Input.inputString) {
                string characterString;
                switch (character) {
                    case '\b':
                        characterString = "Backspace";
                        break;
                    case '\n':
                        characterString = "Enter";
                        break;
                    default:
                        characterString = character.ToString();
                        break;
                }
                handler(this, new KeyboardInputEventArgs(characterString, modifiers));
            }
        }
    }
}
