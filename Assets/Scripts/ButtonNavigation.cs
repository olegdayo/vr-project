﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuplex.WebView;
using UnityEngine.EventSystems;


public class ButtonNavigation : MonoBehaviour
{
    [SerializeField]
    CanvasWebViewPrefab browserPrefab;
    public CanvasWebViewPrefab BrowserPrefab { get; set; }
    public string HomeURL = "https://www.youtube.com";


    public void goHome()
    {
        browserPrefab.WebView.LoadUrl(HomeURL);
    }


    public void changeDragMode()
    {
        if (browserPrefab.DragMode == DragMode.DragToScroll)
            browserPrefab.DragMode = DragMode.DragWithinPage;
        else
            browserPrefab.DragMode = DragMode.DragToScroll;
    }


    public void goBack()
    {
        browserPrefab.WebView.GoBack();
    }


    public void goForward()
    {
        browserPrefab.WebView.GoForward();
    }
}
