﻿ using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EyeGazePointer : BaseEventPointer
{
    public Camera mainCamera;
    public GameObject webView;
    public HandGestureDetector detector;
    public LineRenderer Gaze = null;

    public int dragThreshold;

    LinkedList<Vector3> rayPositions = new LinkedList<Vector3>();
    long lastMiddlePinchTimestamp = -1;

    void FixedUpdate()
    {
        getRayPositions();
        
        ProcessIndexPinch();
        ProcessMiddlePinch();

        LocateObject();
    }

    void ProcessIndexPinch()
    {
        bool pinchedIndex = !detector.getPinchedFingers(1, 0.05f);

        if (lastPinchTimestamp == -1 && pinchedIndex)
        {
            lastPinchTimestamp = DateTimeOffset.Now.ToUnixTimeMilliseconds();
            return;
        }

        if (lastPinchTimestamp != -1 && !pinchedIndex)
        {
            lastPinchTimestamp = -1;
            var raycastresult = GetCurrentRaycast();
            Click(ConvertToScreenCoords(mainCamera, raycastresult.Item1), raycastresult.Item2);
        }
    }

    void ProcessMiddlePinch()
    {
        bool pinchedCorrectionMiddle = detector.getPinchedFingers(2, 0.13f);
        bool pinchedMiddle = detector.getPinchedFingers(2, 0.05f);

        var raycastResult = GetCurrentRaycast();

        if (!pinchedMiddle && dragging)
            EndDrag(ConvertToScreenCoords(mainCamera, raycastResult.Item1), raycastResult.Item2);

        if (lastMiddlePinchTimestamp == -1 && pinchedCorrectionMiddle)
            lastMiddlePinchTimestamp = DateTimeOffset.Now.ToUnixTimeMilliseconds();

        if (pinchedCorrectionMiddle)
        {
            var screenPos = ConvertToScreenCoords(mainCamera, raycastResult.Item1);
            lastPinchedRaycast = raycastResult.Item1;
            detector.setInitialPinch();

            if (pinchedMiddle && !dragging)
                BeginDrag(screenPos, raycastResult.Item2);
        }

        if (dragging)
        {
            var raycastPos = lastPinchedRaycast - detector.getFingerXYOffset(2);
            var screenPos = ConvertToScreenCoords(mainCamera, raycastPos);
            LocateObject(raycastPos);
            Drag(screenPos, raycastResult.Item2);
        }
    }

    void LocateObject(Vector3 position = new Vector3())
    {
        if (position.magnitude == 0)
            transform.position = GetCurrentRaycast().Item1;
        else
            transform.position = position;
    }

    (Vector3, GameObject) GetCurrentRaycast()
    {
        RaycastHit info;
        Physics.Raycast(mainCamera.transform.position, rayPositions.First.Value, out info);

        return (info.point, info.transform.gameObject);
    }

    void getRayPositions()
    {
        Vector3[] positions = new Vector3[2];
        Gaze.GetPositions(positions);

        if (rayPositions.Count >= (50 * 200 / 1000))
            rayPositions.RemoveFirst();

        rayPositions.AddLast(positions[1] - positions[0]);
    }
}
