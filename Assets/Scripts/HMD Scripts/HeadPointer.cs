﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class HeadPointer : MonoBehaviour
{
    [SerializeField]
    Camera cam;
    public Camera Reticle { get; set; }

    [SerializeField]
    GameObject browser;
    public GameObject Browser { get; set; }

    public float scrollRate = 1;
    public float scrollCap = 0.5f;

    GameObject selectedObj;
    bool pressed = false;
    bool dragging = false;

    long timestampPress;
    float scrollmeter = 0.005f;

    void Update()
    {
        PointerEventData eventData = new PointerEventData(EventSystem.current);
        RaycastHit hit;

        if (Physics.Raycast(transform.position, PinchPointer.gazeMode ? PinchPointer.rayPositions.First.Value : transform.forward, out hit))
        {
            var name = hit.transform.gameObject.name;
            if (name == "UpperScroll")
            {
                scrollPageMouse(true, eventData);
                if (scrollRate < scrollCap)
                    scrollmeter *= (1 + scrollRate / 100);//1.01f;
                return;
            }
            else if (name == "BottomScroll")
            {
                scrollPageMouse(false, eventData);
                if (scrollRate < scrollCap)
                    scrollmeter *= (1 + scrollRate / 100);//1.01f;
                return;
            }
        }
        scrollmeter = 0.005f;


        if (Input.GetKeyDown("tab"))
        {
            timestampPress = DateTimeOffset.Now.ToUnixTimeMilliseconds();
        }


        if (Input.GetKeyUp("tab"))
        {
            Debug.Log("Released + " + timestampPress);

            eventData.position = cam.WorldToScreenPoint(hit.point);

            if (DateTimeOffset.Now.ToUnixTimeMilliseconds() - timestampPress <= 100)
            {
                Debug.Log("Click! = " + timestampPress);

                if (selectedObj != null)
                {
                    ExecuteEvents.Execute(selectedObj.name == "Canvas" ? browser : selectedObj, eventData, ExecuteEvents.pointerClickHandler);
                }
            }
            else
            {
                if (dragging)
                {
                    ExecuteEvents.Execute(browser, eventData, ExecuteEvents.endDragHandler);
                    dragging = false;
                    ExecuteEvents.Execute(browser, eventData, ExecuteEvents.pointerUpHandler);
                }
                else
                {
                    Debug.Log("RClick! = " + timestampPress);

                    if (selectedObj != null)
                    {
                        ExecuteEvents.Execute(selectedObj.name == "Canvas" ? browser : selectedObj, eventData, ExecuteEvents.pointerClickHandler);
                    }
                }
            }

            pressed = false;
            timestampPress = 0;
        }


        if (Input.GetKey("tab"))
        {
            if (Physics.Raycast(transform.position, transform.forward, out hit))
            {
                eventData.position = cam.WorldToScreenPoint(hit.point);
                selectedObj = hit.transform.gameObject;

                if (DateTimeOffset.Now.ToUnixTimeMilliseconds() - timestampPress < 100)
                    return;

                if (selectedObj != null)
                {
                    Debug.Log(selectedObj.name);
                    switch (selectedObj.name)
                    {
                        case "Canvas":
                            if (!pressed)
                            {
                                Debug.Log("Start Hold + " + timestampPress);

                                ExecuteEvents.Execute(browser, eventData, ExecuteEvents.pointerDownHandler);
                                ExecuteEvents.Execute(browser, eventData, ExecuteEvents.beginDragHandler);
                            }
                            else
                            {
                                Debug.Log("Dragging + " + timestampPress);
                                dragging = true;
                                ExecuteEvents.Execute(browser, eventData, ExecuteEvents.dragHandler);
                            }
                            selectedObj = browser;
                            break;
                        default:
                            ExecuteEvents.Execute(selectedObj, eventData, ExecuteEvents.pointerClickHandler);
                            selectedObj = null;
                            return;
                    }
                }
            }
            pressed = true;
            Debug.Log("Pressed! + " + timestampPress);
        }
    }

    void checkForScroll()
    {
        if (Input.GetAxis("Mouse ScrollWheel") != 0f)
        {
            var eventData = new PointerEventData(EventSystem.current);
            eventData.scrollDelta = new Vector2(0, Input.GetAxis("Mouse ScrollWheel") / 10);
            ExecuteEvents.Execute(browser, eventData, ExecuteEvents.scrollHandler);
        }
    }

    void scrollPageMouse(bool up, PointerEventData eventData)
    {
        eventData.scrollDelta = new Vector2(0, up ? Math.Min(scrollmeter, scrollCap) : Math.Min(scrollmeter, scrollCap) * (-1)) / 10;
        ExecuteEvents.Execute(browser, eventData, ExecuteEvents.scrollHandler);
    }
}
