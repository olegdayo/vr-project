﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Leap.Unity;
using UnityEngine.EventSystems;
using UnityEngine.UIElements;
using Vuplex.WebView;
using System;
using ViveSR.anipal.Eye;
using Leap;

public class PinchPointer : PinchDetector
{
    static public bool gazeMode;
    static public LinkedList<Vector3> rayPositions = new LinkedList<Vector3>();

    [SerializeField]
    Canvas _canvas;
    public Canvas canvas { get; set; }

    [SerializeField]
    Camera cam;
    public Camera Reticle { get; set; }

    [SerializeField]
    GameObject browser;
    public GameObject Browser { get; set; }

    [SerializeField]
    CanvasWebViewPrefab prefab;
    public CanvasWebViewPrefab Prefab { get; set; }

    [SerializeField]
    //LineRenderer gaze;
    public LineRenderer Gaze = null;

    public AudioSource audioSource;

    public CapsuleHand hand;

    /// <summary>
    /// Threshold for browser mode selection
    /// </summary>
    public long clickMillis = 200;
    public float Sensitivity = 5f;
    public float DragSensetivity = 2.5f;
    public float Period = .1f; 
    public float OnValue = 1.0f;
    public float OffValue = 1.5f;
    /// <summary>
    /// Threshold for drag inertia
    /// </summary>
    public float magnitudeThreshold = 1;
    /// <summary>
    /// If travel disance of hand by clickMillis exceeds this, then scroll. Else - drag
    /// </summary>
    public float travelDistance;
    /// <summary>
    /// Rate of drag inertia
    /// </summary>
    public float decayRate;
    /// <summary>
    /// Delay of pointer to gaze
    /// </summary>
    public float gazeDelayMillis = 200;
    /// <summary>
    /// Distance from index to thumb that triggers a pinch
    /// </summary>
    public float pressDistance = 0.3f;
    /// <summary>
    /// Distance from index to thumb that triggers a hover 
    /// </summary>
    public float hoverDistance = 0.7f;
    public bool isOculusQuest = false;

    RaycastHit initHit;
    bool inert = false;
    bool setMode = false;
    bool dragging;
    long timestamp = -1;
    GameObject selectedObj;
    Vector3 initialPinchTransform;

    Vector3 initialCorrection;
    bool indexPinch;

    Vector3 initialHit;
    Vector3 lastClick;
    Vector3 prevPos;
    Vector3 lastDrag;
    Vector3 currentDragInertia;
    Vector3 oculusOffset = new Vector3(0.15f, -0.5f, -0.2f);
    bool pinched;
    float sensitivity = 5f;
    PointerEventData eData;
    Hand _hand;

    new void Awake()
    {
        if (GetComponent<HandModelBase>() != null && ControlsTransform == true)
        {
            Debug.LogWarning("Detector should not be control the HandModelBase's transform. Either attach it to its own transform or set ControlsTransform to false.");
        }
        if (_handModel == null)
        {
            _handModel = GetComponentInParent<HandModelBase>();
            if (_handModel == null)
            {
                Debug.LogWarning("The HandModel field of Detector was unassigned and the detector has been disabled.");
                enabled = false;
            }
        }

        gazeMode = Gaze != null;
        Debug.Log(gazeMode);
        sensitivity = Sensitivity;
    }

    private void FixedUpdate()
    {
        _hand = hand.GetLeapHand();
        getRayPositions();

        UpdateLocation();

        if (inert)
            inertDrag(false);
        
        if (!pinched)
        {
            return;
        }

        var currentPos = _hand.GetThumb().TipPosition.ToVector3();
        if (!setMode)
        {
            if (!indexPinch)
                setDragMode(currentPos, true);
            else
                setDragMode(Position);
            return;
        }

        lastDrag = drag(indexPinch ? Position : currentPos);
    }

    /// <summary>
    /// Is called when a pinch is detected
    /// </summary>
    public void pinching()
    {
        prefab.DragMode = DragMode.Disabled;
        Debug.Log("Pinch");
        timestamp = DateTimeOffset.Now.ToUnixTimeMilliseconds();

        indexPinch = true;
        if (inert)
            inertDrag(true);

        ExecEvents(Position);    
    }

    /// <summary>
    /// Executes beginDrag events
    /// </summary>
    /// <returns></returns>
    Vector3 drag(Vector3 pinchPos = new Vector3())
    {
        lastClick = new Vector3();
        Vector3 differenceOnPlane;
        if (pinched && selectedObj != null)
        {
            var p = pinchPos;
            lastDrag = p - prevPos;
            prevPos = p;
            differenceOnPlane = p - initialPinchTransform;
            differenceOnPlane.z = 0;

            PointerEventData eventData = new PointerEventData(EventSystem.current);

            Vector3 hitPos = initHit.point/*initialHit*/ + differenceOnPlane * sensitivity;
            eventData.position = cam.WorldToScreenPoint(hitPos);

            if (!dragging)
            {
                Debug.Log("Init drag");
                ExecuteEvents.Execute(browser, eData, ExecuteEvents.pointerDownHandler);
                ExecuteEvents.Execute(selectedObj, eventData, ExecuteEvents.beginDragHandler);
                dragging = true;
            }
            //Debug.Log("Dragging Pinch " + pinchPos);
            ExecuteEvents.Execute(selectedObj, eventData, ExecuteEvents.dragHandler);
        }
        return lastDrag;
    }

    /// <summary>
    /// Continues to scroll with inertia
    /// </summary>
    /// <param name="stop"></param>
    void inertDrag(bool stop)
    {
        currentDragInertia /= (1 + decayRate / 100);
        PointerEventData eventData = new PointerEventData(EventSystem.current);
        Vector3 differenceOnPlane = new Vector3(0, 0, 0);
        lastDrag += currentDragInertia * sensitivity;
        eventData.position = cam.WorldToScreenPoint(initialHit + lastDrag);

        if (lastDrag.magnitude < magnitudeThreshold && currentDragInertia.magnitude > 0.001f && !stop)
        {
            if (!dragging)
            {
                ExecuteEvents.Execute(selectedObj, eventData, ExecuteEvents.beginDragHandler);
                dragging = true;
            }
            ExecuteEvents.Execute(selectedObj, eventData, ExecuteEvents.dragHandler);
        }
        else
        {
            ExecuteEvents.Execute(selectedObj, eventData, ExecuteEvents.endDragHandler);
            dragging = false;
            inert = false;
        }
    }

    /// <summary>
    /// Executes pointerDown, startDrag events
    /// </summary>
    void ExecEvents(Vector3 pinchPos = new Vector3())
    {
        if (dragging)
            return;

        PointerEventData eventData = new PointerEventData(EventSystem.current);

        RaycastHit hit;
        initialPinchTransform = pinchPos;

        inert = false;

        Vector3[] positions = new Vector3[3];
        if (gazeMode)
            Gaze.GetPositions(positions);

        Vector3 sourcePoint = cam.transform.position;;
        if (isOculusQuest)
            sourcePoint += oculusOffset;

        if (Physics.Raycast(sourcePoint, transform.position - sourcePoint, out hit))
        {
            Debug.Log("Grabbed Pinch");
            eventData.position = cam.WorldToScreenPoint(hit.point);
            selectedObj = hit.transform.gameObject;
            initialHit = hit.point;
            initHit = hit;

            if (selectedObj != null)
                switch (selectedObj.name)
                {
                    case "Canvas":
                        selectedObj = browser;
                        break;
                    default:
                        ExecuteEvents.Execute(selectedObj, eventData, ExecuteEvents.pointerClickHandler);
                        selectedObj = null;
                        break;
                }
            pinched = true;
        }

        eData = eventData;
    }

    /// <summary>
    /// Function on release of pinch
    /// When pinch is dropped, executes PointerUp, endDrag and Click events
    /// </summary>
    public void dropPinch(Vector3 pinchPos = new Vector3())
    {
        var p = indexPinch ? Position : pinchPos;
        timestamp = -1;
        Debug.Log("Released Pinch");
        pinched = false;

        if (!setMode)
        {
            sendClick(initialHit);
            lastClick = initialHit;
            Debug.Log("Click!");
        } else
        {
            Vector3 diff = p - initialPinchTransform;
            diff.z = 0;

            PointerEventData eventData = new PointerEventData(EventSystem.current);
            eventData.position = cam.WorldToScreenPoint(initialHit);

            if (dragging)
            {
                eventData.position = cam.WorldToScreenPoint(initialHit + diff * sensitivity);
                ExecuteEvents.Execute(browser, eventData, ExecuteEvents.endDragHandler);
                ExecuteEvents.Execute(browser, eventData, ExecuteEvents.pointerUpHandler);
                dragging = false;
                if (prefab.DragMode == DragMode.DragToScroll)
                    inert = true;
            }
        }
        if (gazeMode && !indexPinch)
            prefab.DragMode = DragMode.Disabled;
        currentDragInertia = lastDrag;
        indexPinch = false;
        initialPinchTransform = new Vector3();
        prevPos = new Vector3();
        setMode = false;
    }

    /// <summary>
    /// Sets a browser to either scroll mode or drag mode 
    /// </summary>
    void setDragMode(Vector3 pinchPos = new Vector3(), bool setDrag = false)
    {
        var p = pinchPos;
        if (Math.Abs((p - initialPinchTransform).magnitude) > travelDistance && indexPinch)
        {
            initialPinchTransform = p;
            prefab.DragMode = DragMode.DragToScroll;
            sensitivity = Sensitivity;
            setMode = true;
            return;
        }

        if (setDrag || (Math.Abs((p - initialPinchTransform).magnitude) < travelDistance &&
             gazeMode ? !indexPinch : (DateTimeOffset.Now.ToUnixTimeMilliseconds() - timestamp > clickMillis)))
        {
            prefab.DragMode = DragMode.DragWithinPage;
            audioSource.Play();
            sensitivity = DragSensetivity;
            setMode = true;
        }
    }

    public float yOffset = -0.5f;
    Vector3 tr = new Vector3(0, -5, 0);

    /// <summary>
    /// Gets a transofrm of a Raycast from Gaze direction
    /// </summary>
    void UpdateLocation()
    {
        Vector3[] positions = new Vector3[3];
        if (gazeMode)
            Gaze.GetPositions(positions);

        if (driveLocationWithPinch())
        {
            return;
        }

        Vector3 sourcePoint = cam.transform.position; 
        if (isOculusQuest)
            sourcePoint += oculusOffset;
        Vector3 midpos = (_hand.GetThumb().TipPosition.ToVector3() + _hand.GetThumb().TipPosition.ToVector3()) / 2;

        // This happens when fingers are not in correction area
        RaycastHit hit;
        if (Physics.Raycast(sourcePoint, gazeMode ? rayPositions.First.Value :
            (isOculusQuest ? _hand.GetThumb().TipPosition.ToVector3() - sourcePoint : cam.transform.forward), out hit))
        {
            Vector3 hitPoint = hit.point;
            if (gazeMode) hitPoint.y += yOffset;
            transform.position = hitPoint;
        }
        else
            transform.position = tr;
    }

    /// <summary>
    /// Adds to double-linked list a gaze ray vector each frame
    /// </summary>
    void getRayPositions()
    {
        if (!gazeMode)
            return;

        Vector3[] positions = new Vector3[2];
        Gaze.GetPositions(positions);

        if (rayPositions.Count >= (int)(50 * gazeDelayMillis / 1000))
            rayPositions.RemoveFirst();

        rayPositions.AddLast(positions[1] - positions[0]);
    }

    void sendClick(Vector3 pos)
    {
        float minX = _canvas.GetComponent<RectTransform>().position.x + _canvas.GetComponent<RectTransform>().rect.xMin;
        float maxY = _canvas.GetComponent<RectTransform>().position.y + _canvas.GetComponent<RectTransform>().rect.yMax;
        float width = _canvas.GetComponent<RectTransform>().rect.width;
        float height = _canvas.GetComponent<RectTransform>().rect.height;

        Debug.Log(new Vector2((pos.x - minX) / width, (maxY - pos.y) / height));

        prefab.WebView.Click(new Vector2((pos.x - minX) / width, (maxY - pos.y) / height));
    }

    bool driveLocationWithPinch()
    {
        if (!gazeMode || _hand == null || indexPinch)
            return false;

        Vector thumbPos = _hand.GetThumb().TipPosition;
        Vector middlePos = _hand.GetMiddle().TipPosition;
        var dist = thumbPos.DistanceTo(middlePos);

        if (dist > pressDistance && dist < hoverDistance)
        {
            if (initialCorrection.magnitude == 0)
            {
                audioSource.Play();
                initialCorrection = thumbPos.ToVector3();
            }
            var offset = initialCorrection - thumbPos.ToVector3();
            initialCorrection = thumbPos.ToVector3();
            offset.z = 0;
            offset *= sensitivity;
            transform.position -= offset;
            return true;
        }

        if (dist <= pressDistance)
        {
            Debug.Log("Pinch");
            //timestamp = 0;
            
            //if (inert)
            //    inertDrag(true);

            //if (!setMode)
            //{
                ExecEvents(thumbPos.ToVector3());
            //    setDragMode(thumbPos.ToVector3(), true);
            //}
            var offset = initialCorrection - thumbPos.ToVector3();
            initialCorrection = thumbPos.ToVector3();
            offset.z = 0;
            offset *= sensitivity;
            transform.position -= offset;
            return true;
        }

        if (pinched)
            dropPinch(thumbPos.ToVector3());

        initialCorrection = new Vector3();
        return false;
    }





    // no....

    public void dropPinch()
    {
        timestamp = -1;
        Debug.Log("Released Pinch");
        pinched = false;

        if (!setMode)
        {
            sendClick(initialHit);
            lastClick = initialHit;
            Debug.Log("Click!");
        }
        else
        {
            Vector3 diff = Position - initialPinchTransform;
            diff.z = 0;

            PointerEventData eventData = new PointerEventData(EventSystem.current);
            eventData.position = cam.WorldToScreenPoint(initialHit);

            if (dragging)
            {
                eventData.position = cam.WorldToScreenPoint(initialHit + diff * sensitivity);
                ExecuteEvents.Execute(browser, eventData, ExecuteEvents.endDragHandler);
                ExecuteEvents.Execute(browser, eventData, ExecuteEvents.pointerUpHandler);
                dragging = false;
                if (prefab.DragMode == DragMode.DragToScroll)
                    inert = true;
            }
        }

        currentDragInertia = lastDrag;
        indexPinch = false;
        initialPinchTransform = new Vector3();
        prevPos = new Vector3();
        setMode = false;
    }
}

