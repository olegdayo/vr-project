﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UIElements;
using UnityEngine.XR;
using Leap.Unity;

public class PointerLocator : MonoBehaviour
{
    [SerializeField]
    Camera cam;
    public Camera Cam { get { return cam; } set { cam = value; } }

    [SerializeField]
    private GameObject browser;
    public GameObject Browser { get { return browser; } set { browser = value; } }

    public int sensetivity = 20;

    void Update()
    {
        transform.position += new Vector3(Input.GetAxis("Mouse X") * Time.deltaTime * sensetivity, Input.GetAxis("Mouse Y") * Time.deltaTime * sensetivity, 0);
    }

}
