﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Vuplex.WebView;

public class BrowserModeVisualizer : MonoBehaviour
{
    public Button _button;

    public CanvasWebViewPrefab Prefab;

    void Update()
    {
        ColorBlock cb = _button.colors;
        switch (Prefab.DragMode)
        {
            case DragMode.Disabled:
                cb.normalColor = Color.white;
                break;
            case DragMode.DragToScroll:
                cb.normalColor = Color.yellow;
                break;
            case DragMode.DragWithinPage:
                cb.normalColor = Color.green;
                break;
        }
        _button.colors = cb;
    }
}
